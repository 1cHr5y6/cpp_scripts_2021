#include <iostream>
#include <string>
#include "Menu.h"
#include "Reverse.h"

using namespace std;

int Menu::menu_options() {
	cout << "Choose from the following menu: " << endl;
	cout << "-------------------------------" << endl;
	cout << endl;
	cout << "1. Reverse a number." << endl;
	cout << "2. Reverse a string." << endl;
	cout << endl;
	cout << "Enter your selection:  " << flush;
	int input;
	cin >> input;
	return input;
}

int Menu::menu_choice(int choice) {
	switch (choice) {
		case 1:
		{
			cout << "You have selected to reverse a number: " << endl;
			Reverse reverse_a;
			reverse_a.reverse_integer();
			break;
		}
		case 2:
		{
			cout << "You have selectd to reverse a string: " << endl;
			Reverse reverse_b;
			reverse_b.reverse_string();
			break;
		}
		default:
			cout << "Invalid selection. Enter a selection." << endl;
		
	} 
	return choice;
}


