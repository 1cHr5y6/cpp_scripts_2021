#include <iostream>
#include <string>
#include "Banners.h"
#include "Input.h"

using namespace std;

int main()
{

	// Set color theme
	system("color 0B");

	// Main banner
	Banners banners;
	banners.main();

	// User input and reverse user input
	Input input;
	input.echo_and_reverse();


	return 0;
}


