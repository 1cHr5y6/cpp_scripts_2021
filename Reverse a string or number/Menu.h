#ifndef MENU_H_
#define MENU_H_
#include "Reverse.h"

using namespace std;
extern int menu_choice;

class Menu {
public:
	int menu_options();
	// Include parameter choice for menu_choice switch statement 
	int menu_choice(int choice); 
};

#endif /* MENU_H_ */ 
