#include <iostream>
#include <string>
#include "Input.h"

using namespace std;

void Input::echo_and_reverse() {
	string input;

	cout << "Enter a string to reverse: " << endl;
	getline(cin, input);

	cout << endl;
	cout << "You entered: " << endl;
	int i;
	for (i = 0; i < input.length(); i++) {
		cout << input.at(i) << flush;
	}

	cout << endl;
	cout << endl;
	cout << "The reverse string: " << endl;
	for (i = input.length() - 1; i >= 0; i--) {
		cout << input.at(i) << flush;
	}
	cout << endl;
	cout << endl;

}
