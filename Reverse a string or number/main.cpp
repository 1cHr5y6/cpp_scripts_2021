// main.cpp

#include <iostream>
#include "Banners.h"
#include "Menu.h"
#include "Clear.h"
#include "Reverse.h"

using namespace std;

int main()
{
	// Set color theme
	system("color 0B");

	// Main banner
	Banners banners;
	banners.main();

	// Clear screen
	Clear clear;
	clear.clear_screen();

    // Offer menu selection
	Menu menu;
	int selection = menu.menu_options();
	menu.menu_choice(selection);


	return 0;
}

