#include <iostream>
#include <string>
#include "Reverse.h"

using namespace std;

void Reverse::reverse_string() {
	string input;

	cout << "Enter a string to reverse: " << endl;
	getline(cin, input);
	getline(cin >> ws, input); // remove all leading whitespaces

	cout << endl;
	cout << "You entered: " << endl;
	int i;
	for (i = 0; i < input.length(); i++) {
		cout << input.at(i) << flush;
	}

	cout << endl;
	cout << endl;
	cout << "The reverse string is: " << endl;
	for (i = input.length() - 1; i >= 0; i--) {
		cout << input.at(i) << flush;
	}

	cout << endl;
	cout << endl;

	return;
}

int Reverse::reverse_integer() {
	int n, reversedNumber = 0, remainder;

	cout << "Enter a positive whole number: " << flush;
	cin >> n;

	while (n != 0) {
		remainder = n % 10;
		reversedNumber = reversedNumber * 10 + remainder;
		n /= 10;
	}

	cout << "The reversed number is: " << reversedNumber << endl;

	return reversedNumber;

}








