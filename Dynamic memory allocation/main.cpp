#include <iostream>

using namespace std;

class Book {
private:
	// Instance variable
	string title;
public:
	// Constructor
	Book() {
		cout << "Book created." << endl;
	}

	// Copy Constructor
	Book(const Book& other) :
		title(other.title) {
		cout << "Book created by copying." << endl;
	}

	// Destructor
	~Book() {
		// Memory is deallocated.
		cout << "Destructor called." << endl;
	}

	// Set title method
	void setTitle(string title) {
		this->title = title;
	}

	// Output title method
	void display() const {
		cout << "The title of the book is: " << title << endl;
	}
};

Book* createBook() {
	Book* a = new Book;
	a->setTitle("The History of Ancient Greece.");
	return a;
}

int main()
{
	// Create an object from a class.
	// Declare a variable (book) of the type class (Book)
	cout << "Creating an object from a class: " << endl;
	cout << "-------------------------------- " << endl;
	Book book;
	book.setTitle("The History of the Ancient World.");
	book.display();
	cout << endl;

	// Cannot call set title on a pointer, only an object.
	// The . operator has higher precedence than the * operator.
	cout << "Another way of instantiating an object from a class: " << endl;
	cout << "-------------------------------- " << endl;
	// The new operator is allocating memory to a new object (Book())
	Book* pBook1 = new Book();
	pBook1->setTitle("Greece and Rome: An Integrated History of the Mediterranean.");
	pBook1->display();

	// When you allocate memory with new, deallocate the memory explicitly.
	cout << "pBook1's destructor" << endl;
	delete pBook1;

	
	cout << sizeof(pBook1) << endl;
	cout << sizeof(long(pBook1)) << "\n" << endl;;

	// Destructor called for instance variable (book) of class (Book).
	// This is the last line to be displayed in the terminal.

	return 0;
}
