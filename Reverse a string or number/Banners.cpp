#include <iostream>
#include <conio.h>
#include "Banners.h"

using namespace std;

void Banners::main() {
	cout << "==========================================" << endl;
	cout << "======                              ======" << endl;
	cout << "======         Reverse Menu         ======" << endl;
	cout << "======                              ======" << endl;
	cout << "==========================================" << endl;
	cout << endl;
	cout << "Press any key to continue.." << endl;

	cin.clear();
	cin.get();

	system("clr");

}

