#ifndef REVERSE_H_
#define REVERSE_H_
#include <string>

class Reverse {
public:
	void reverse_string();
	int reverse_integer();
};

#endif /* REVERSE_H_ */ 


